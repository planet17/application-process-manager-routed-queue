<?php

namespace Planet17\ApplicationProcessManagerRoutedQueue\Handlers;

use Planet17\ApplicationProcessManagerRoutedQueue\Core\ApplicationProcessManager;
use Planet17\ApplicationProcessManagerRoutedQueue\Processes\CliBackgroundPhpProcess;
use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections\ManagerInterface;
use Planet17\MessageQueueProcessManager\DTO\ProcessManagerDTO;
use Planet17\MessageQueueProcessManager\Messages\ProcessManagerMessage;

/**
 * Class ProcessManagerHandler
 *
 * @package Planet17\ApplicationProcessManagerRoutedQueue\Handlers
 */
abstract class ProcessManagerHandler extends \Planet17\MessageQueueProcessManager\Handlers\ProcessManagerHandler
{
    /** @inheritdoc  */
    protected function getConnectionManager(): ManagerInterface
    {
        return ApplicationProcessManager::getInstance()->getConnectionManager();
    }

    /**
     * Method encapsulated creation of new process and return `pid`.
     *
     * @param HandlerInterface $handler
     *
     * @return int Process ID (pid).
     */
    protected function makeNewProcess(HandlerInterface $handler): int
    {
        return (new CliBackgroundPhpProcess($handler->getRoute()->getAliasFull()))->execute()->getOutput();
    }

    /** @inheritdoc */
    protected function makeSelfMessage(ProcessManagerDTO $dto): MessageInterface
    {
        return new ProcessManagerMessage($dto);
    }
}
