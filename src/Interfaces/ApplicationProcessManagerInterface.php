<?php

namespace Planet17\ApplicationProcessManagerRoutedQueue\Interfaces;

use Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections\ManagerInterface as ConnectionManagerInterface;

/**
 * Interface ApplicationProcessManagerInterface
 *
 * @package Planet17\ApplicationProcessManagerRoutedQueue\Interfaces
 */
interface ApplicationProcessManagerInterface
{
    /**
     * Method for public access for instance __construct.
     *
     * @return ApplicationProcessManagerInterface
     */
    public static function getInstance(): ApplicationProcessManagerInterface;

    /**
     * Core method for start working application: processing
     */
    public function initialize(): void;

    /**
     * Getter method for return provided option in the cli.
     *
     * @return string
     */
    public function getCalledQueueRoute(): string;

    /**
     * Method implement return of option name will be reading from cli.
     *
     * @return string
     */
    public function getCliParamLetter(): string;

    /**
     * Method must implemented return of instance custom connection manager.
     *
     * @return ConnectionManagerInterface
     */
    public function getConnectionManager(): ConnectionManagerInterface;

    /**
     * Method must implement fetching configurations.
     *
     * @return array
     */
    public function fetchConfigurations():array;
}
