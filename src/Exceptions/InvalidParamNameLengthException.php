<?php

namespace Planet17\ApplicationProcessManagerRoutedQueue\Exceptions;

use LogicException;
use Throwable;

/**
 * Class InvalidParamNameLengthException
 *
 * @package Planet17\ApplicationProcessManagerRoutedQueue\Exceptions8
 */
class InvalidParamNameLengthException extends LogicException
{
    /** @const DEFAULT_MESSAGE */
    public const DEFAULT_MESSAGE = 'invalid param name length exception';

    /**
     * InvalidParamNameLengthException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::DEFAULT_MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
