<?php

namespace Planet17\ApplicationProcessManagerRoutedQueue\Processes;

use Planet17\ApplicationProcessManagerRoutedQueue\Core\ApplicationProcessManager;
use Planet17\CLIProcesses\Processes\BackgroundProcess;

/**
 * Class CliBackgroundPhpProcess
 *
 * @package Planet17\ApplicationProcessManagerRoutedQueue\Processes
 */
class CliBackgroundPhpProcess extends BackgroundProcess
{
    /** @inheritdoc  */
    public function getCommand(): string
    {
        $entryPoint = $this->getEntryPointPath();
        $optionName = $this->extractCliParamLetter();
        $php = $this->getSystemPhpPath();
        $parentCommand = parent::getCommand();

        return "{$php} {$entryPoint} -{$optionName} {$parentCommand}";
    }

    /**
     * Method for extracting right option name.
     *
     * @return string
     */
    public function extractCliParamLetter(): string
    {
        return ApplicationProcessManager::getInstance()->getCliParamLetter();
    }

    /**
     * Getter name|path to cli php executable.
     *
     * @return string
     */
    public function getSystemPhpPath():string
    {
        return 'php';
    }

    /**
     * Getter name|path to cli entry point for run another process of application.
     *
     * Override that whether different path is need.
     *
     * @return string
     */
    public function getEntryPointPath():string
    {
        return 'public/index.php';
    }
}
