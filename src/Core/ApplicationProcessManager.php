<?php

namespace Planet17\ApplicationProcessManagerRoutedQueue\Core;

use Planet17\ApplicationProcessManagerRoutedQueue\Exceptions\InvalidParamNameLengthException;
use Planet17\ApplicationProcessManagerRoutedQueue\Interfaces\ApplicationProcessManagerInterface;
use Planet17\MessageQueueProcessManager\Routes\ProcessManagerRoute;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections\ManagerInterface as ConnectionManagerInterface;

/**
 * Class ApplicationProcessManager
 *
 * @package Planet17\ApplicationProcessManagerRoutedQueue
 */
abstract class ApplicationProcessManager implements ApplicationProcessManagerInterface
{
    /** @const DEFAULT_CLI_PARAM_LETTER */
    public const DEFAULT_CLI_PARAM_LETTER = 'q';

    /** @var ApplicationProcessManager */
    protected static $instance;

    /** @var string */
    protected $calledQueueRoute;

    /**
     * ApplicationProcessManager constructor.
     *
     * Deny allow from outside or from extended classes.
     */
    private function __construct()
    {
        $this->validateCliParamLetter();
        $this->initializeConnectionsSetup();
    }

    /** @inheritdoc */
    public static function getInstance(): ApplicationProcessManagerInterface
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    /** @inheritdoc */
    public function initialize(): void
    {
        $this->fetchParams();
        $this->getConnectionManager()
            ->getResolverAliasHandler()
            ->resolveHandlerByRouteAlias($this->getCalledQueueRoute())
            ->initialize();
    }

    /** @inheritdoc */
    public function getCalledQueueRoute(): string
    {
        return $this->calledQueueRoute;
    }

    /** @inheritdoc */
    public function getCliParamLetter(): string
    {
        return self::DEFAULT_CLI_PARAM_LETTER;
    }

    /** @inheritdoc */
    abstract public function getConnectionManager(): ConnectionManagerInterface;

    /** @inheritdoc */
    abstract public function fetchConfigurations():array;

    /**
     * Method run fetching configurations for connections and append it to connection manager instance.
     */
    private function initializeConnectionsSetup(): void
    {
        foreach ($this->fetchConfigurations() as $key => $configuration) {
            $this->getConnectionManager()->addConnection($configuration, $key);
        }

        $this->getConnectionManager()->bootRoutes();
    }

    /**
     * Method validate value return by ApplicationProcessManagerInterface::getCliParamLetter()
     *
     * @see ApplicationProcessManagerInterface::getCliParamLetter()
     */
    protected function validateCliParamLetter(): void
    {
        if (strlen($this->getCliParamLetter()) !== 1) {
            throw new InvalidParamNameLengthException('Param name length must been equal one symbol.');
        }
    }

    /**
     * Method contains rules for extracting params from cli.
     */
    protected function fetchParams(): void
    {
        $alias = getopt($this->getCliParamLetter(). ':')[$this->getCliParamLetter()] ?? ProcessManagerRoute::ALIAS;
        $this->calledQueueRoute = $alias;
    }
}
